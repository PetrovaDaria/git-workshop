#include "pitches.h"
#include "button.h"
#include "buzzer.h"

#define PIN_BUTTON_SELECT 3
#define PIN_BUTTON_OFF 5
#define PIN_BUTTON_SPEED 4
#define PIN_BUZZER 6

Button buttonMelodySelect(PIN_BUTTON_SELECT);
Button buttonOff(PIN_BUTTON_OFF);
Button buttonSpeed(PIN_BUTTON_SPEED);
Buzzer buzzer(PIN_BUZZER);

int notes[] = {NOTE_A4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations[] = {8, 1, 4, 1};
int melodyLength = 4;

unsigned long speeds[] = {25, 50, 100, 200, 400, 800};
int currentSpeed = 2;
int speedsLength = 6;

// and the second melody
int notes2[] = {NOTE_C4, NOTE_SILENCE, NOTE_G4, NOTE_SILENCE};
double durations2[] = {4, 1, 4, 1};
int melodyLength2 = 4;

// 0 -NOTE_C5, 1-NOTE_D5 , 2-NOTE_E5 , 3- NOTE_F5, 4- NOTE_G5, 5-NOTE_A5, 6-NOTE_B5, 7-NOTE_C6

int nosaNotes[] = {NOTE_C5, NOTE_G5, NOTE_C6, NOTE_F5, NOTE_G5, NOTE_C6,
NOTE_D5, NOTE_G5, NOTE_C6, NOTE_G5, NOTE_A5, NOTE_G5, NOTE_E5, NOTE_C5,
NOTE_G5, NOTE_C6, NOTE_F5, NOTE_G5, NOTE_C6, NOTE_A5,
NOTE_G5, NOTE_A5, NOTE_G5, NOTE_E5,
NOTE_C5, NOTE_F5,
NOTE_G5, NOTE_D5, NOTE_G5, NOTE_D5, NOTE_F5, NOTE_D5,
NOTE_F5, NOTE_E5, NOTE_F5,
NOTE_G5, NOTE_F5,
NOTE_D5, NOTE_G5, NOTE_D5, NOTE_F5, NOTE_D5,
NOTE_F5, NOTE_E5, NOTE_F5};

double nosaDurations[] = {2, 1, 1, 2, 1, 1, 2, 1, 1, 1, 1, 1, 1, 3, 1, 1, 2, 1, 1, 3,
1, 1, 1, 1, 2, 3, 1, 1, 1, 2, 1, 1, 1, 2, 1, 1, 2, 3, 4, 4, 3, 1, 1, 1, 1, 1, 1, 1, 2, 3, 4};
int nosaMelodyLength = 46;

int currentMelody = 0;
int melodiesLength = 3;

void setup()
{
    buzzer.setMelody(notes, durations, melodyLength);
}

void loop()
{
    buzzer.playSound();

    if (buttonOff.wasPressed())
    {
        buzzer.turnSoundOff();
    }

    if (buttonMelodySelect.wasPressed())
    {
        if (currentMelody == 0)
        {
          buzzer.setMelody(notes, durations, melodyLength);
          buzzer.turnSoundOn();
        }
        if (currentMelody == 1)
        {
          buzzer.setMelody(notes2, durations2, melodyLength2);
          buzzer.turnSoundOn();
        }
        if (currentMelody == 2)
        {
            buzzer.setMelody(nosaNotes, nosaDurations, nosaMelodyLength);
            buzzer.turnSoundOn();
        }
        currentMelody = (currentMelody + 1)%melodiesLength;
      }
        
   
    if (buttonSpeed.wasPressed())
    {
        currentSpeed = (currentSpeed + 1)%speedsLength;
        buzzer.setNoteDuration(speeds[currentSpeed]);
    }
}
